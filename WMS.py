#  -*-coding: utf-8 -*-

# - # - # - # - # - # - # - # - # - # - # - #
#        HRDPS time series Downloader       #
#          Converts WMS Data for use        #
#                                           #
#     Created by Alexandre Leca @ IRDA      #
#   HRDPS by Stephane Belair et al. @ CMC   #
#        last edit : February 22 2016       #
#             -----------------             #
#    IMPORTANT NOTE: this code is stable    #
#    but not heavily tested yet for         #
#    systematic use.                        #
#   Contact alexandre.leca@irda.qc.ca for   #
#      more info and specific details.      #
# - # - # - # - # - # - # - # - # - # - # - #

# Libraries import
import urllib2 # call and read URL
import datetime # date/time formats management
import time # operations on time
import sys
import csv
import os
import math as ma
import collections # manage lists and dictionaries
import fileinput # manage file to find/replace lines (update data)
import webbrowser # control web browser from python
from os.path import exists
import platform # Controls the platform (Windows, Linux, Mac OS,..)
import Mailert as ml

# GETS Arguments for Input/Output file name and path
args = []
for arg in sys.argv:
    args.append(arg)
Path = args[1] # path to read and write, in the form : P:\pomme\Projets\mouillure\Programmation\HRDPS_Parser\
DatOVR = args[2] # argument to override date selection. If no override, set DatOVR to OK, else Enter date to a format YYYY-mm-ddTHH
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Functions used
def ReqPaste(layer,date,lat,lon,elev,nh,step,v):
    """
        This function gets coordinates info and pastes them into a working
        request for the HRDPS WMS
        layer = model and variable (ex: HRDPS.SURF_TJ for HRDPS surface temperature)
        date = date in format YYYYMMDDHH (ex: 2015121712)
        lat = latitude in decimal format (ex: 45.50)
        lon = longitude in decimal format (ex: -71.54)
        elev = point elevation in meters (ex: 125)
        nh = number of hours from starting date (ex: 24 for the next 24 hours forecast)
        step = time step in minutes (ex: 30)
        v = tool version (ex: version=1.1.1)
    """

    Request = "http://collaboration.cmc.ec.gc.ca/rpn-wms/?&"+v+ \
              "&request=GetFeatureInfo&query_layers="+layer+ \
              "&info_format=text/plain&time="+ date + \
              "&ext_duration=pt"+str(nh)+"h&ext_durationstep=pt"+str(step)+ \
              "m&lat="+str(lat)+"&lon="+str(lon)
    return Request

def URLgrab(URL):
    """
        Function to open file from the URL request produced
    """
    RQ = urllib2.Request(URL)
    try:
        handle = urllib2.urlopen(RQ)
        thepage = handle.read() # Read the URL page
        msg = "OK"
        #print URL+" treatment complete"
    except urllib2.HTTPError,e:
        print "{0} does not seem to exist : {1} - {2}".format(URL,e.code,e.msg)
        msg = "URL Access Error {0}".format(e.code)
        thepage = ""
    return thepage,msg

def chequal(lst):
    """
        Fastest equality check function
        Source : http://stackoverflow.com/q/3844948/
    """
    return not lst or lst.count(lst[0]) == len(lst)

def tmconv(tm,U2L):
    """
        Convert time from UTC to local or local to UTC
        parameter U2L is UTC2Local boolean to set the conversion direction
        if U2L is set to True, then time tm has to be UTC and will be converted to local time
        if U2L is set to False, tm has to be local and converted to UTC
    """
    epoch = time.mktime(tm.timetuple())
    offset = datetime.datetime.fromtimestamp (epoch) - datetime.datetime.utcfromtimestamp (epoch)
    if U2L==True:
        return tm + offset
    else:
        return tm - offset

def VisuWeb(URL):
    """
        Could be used to visualize the time series or to visualize the 2D maps
    """
    try:
        #webbrowser.open_new(URL) # open URL in a new window
        webbrowser.open_new_tab(URL) # open URL in a new web browser tab
    except webbrowser.Error,e:
        print "A problem occured with your web browser control, please try to copy" + \
               URL + "manually in your web browser."
# # # # # # # # # # # # # # # # # # # # # # # # # # #

# Initialization of request variables
Version = "version=1.1.1"
#dateo="2015-12-19T00" # control test
#lat=45.54 # control test
#lon=-73.34 # control test
#nhour=48 # hours # control test
step=60 # minutes
varlist = ["TT","HR","PR0","UU","FB","FSD","FSF"] # list of variables to download
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Main program
if platform.system()=='Windows':
    InFile = Path+"\VStations.dat" # File containing the virtual stations coordinates
else:
    InFile = Path+"/VStations.dat" # File containing the virtual stations coordinates

# FOLLOWING CODE WILL BE PUT IN THE FILE OPEN LOOP BELOW AFTER TEST SESSIONS
if exists(InFile):

    # Log File initialization
    errdat = "OK" # Error message related to date and/or data. If OK there is no error
    errvar = "OK" # Error message related to variables count. If OK there is no error
    mailerrors = {} # Dictionnary to store all the errors encountered during the download
    # NOTE : mailerrors will bey keyed by the station ID and contain the date of interest and the error message
    if platform.system()=='Windows':
        logfile = Path+"\download.log"
    else:
        logfile = Path+"/download.log"

    if not exists(logfile):
        with open(logfile,'w') as L:
            L.write("HRDPS MWS Download log\nReports every download process to track errors.\n")
    with open(logfile,'a') as L:
            L.write("\n/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-\n")
            L.write("File download started on "+ \
                    datetime.datetime.strftime(datetime.datetime.now(),"%Y/%m/%d at %H:%M:%S") + \
                    "(local time)\n")

    # Start reading for all stations
    with open(InFile,'r') as F:
        for i in csv.reader(F):
            """
                File's data rows Format is :
                ID,lat,lon,elev
                where :
                ID = string used for the station file name, linked with location info
                lat = latitude (decimal, sign sensitive)
                lon = longitude (decimal, sign sensitive)
                elev = elevation of the point (in meters) not used for now
            """
            if i[0].startswith("#"): # Discards non data rows
                continue
            staname = str(i[0])
            if platform.system()=='Windows':
                OuF = Path+"\Stations\\"+str(i[0])+"_FCT.dat" # Output filename
            else:
                OuF = Path+"/Stations/"+str(i[0])+"_FCT.dat" # Output filename

            lat = i[1]
            lon = i[2]

            # Data retrieval :
            if exists(OuF):
                with open(OuF,'r+') as O:
                    try:
                        rd = collections.deque(csv.reader(O,delimiter=","),1)[0] # gets last line of last file record
                        startime = tmconv(datetime.datetime.strptime(rd[0]+","+rd[1], "%Y/%m/%d,%H:%M"),False)
                        errdat = "OK"
                    except IndexError: # if file exists but is empty
                        O.write("DATE,TIME,AIRTEMP,RELHUM,RAIN,WINDSPEED,RGLOB,RDIFFUSE,RDIRECT\n") # generic data file
                        startime = datetime.datetime.utcnow()
                    except ValueError: #if wrong data is in the file i.e. cannot read proper date format
                        """
                            rather than not adding anything to the file, if update cannot be made from an existing date,
                            an error line will be added to the file, and weather data is updated from the current time
                        """
                        O.write("Wrong data found above this line on {0} \n".format(datetime.datetime.utcnow()))
                        errdat = "Data error before {0}".format(datetime.datetime.utcnow())
                        startime = datetime.datetime.utcnow()
                        mailerrors[staname] = [errdat,datetime.datetime.utcnow().strftime("%Y/%m/%d at %H:%M:%S")]
            else:
                with open(OuF,'w+') as O:
                    O.write("DATE,TIME,AIRTEMP,RELHUM,RAIN,WINDSPEED,RGLOB,RDIFFUSE,RDIRECT\n") # generic data file
                    startime = datetime.datetime.utcnow()

            if DatOVR=="OK":
                dateo = startime.strftime("%Y-%m-%dT%H") # Format the date to match request requirements
            else:
                dateo = datetime.datetime.strptime(DatOVR,"%Y-%m-%dT%H").strftime("%Y-%m-%dT%H") # manual control


            lastdl=int(((datetime.datetime.utcnow()-startime).total_seconds())/3600.)
            nhour = 48 + lastdl # number of hours to compute from HRDPS

            varstock = {} # Time/Values storage dictionnary
            Output = {} # Final dictionary containing ordered data

            for vi in varlist:
                varstock[vi] = {} # will store variable infos

                RQ = ReqPaste("HRDPS.FORCING_"+vi,str(dateo),lat,lon,0,nhour,step,Version) # create request
                RS,logmsg = URLgrab(RQ) # call request URL and get the internet error if one occurred

                co=0 # line counter in data stream
                if logmsg=="OK":
                    for i in RS.splitlines():
                        co=co+1 if co<12 else 1 # to count in files
                        if co==5:
                            vari = i[6:] # variable Values
                        if co==11: # corresponds to the variable value date line
                            if i[5:21] in varstock[vi]:
                                varstock[vi].pop(i[5:21], None) # avoid duplicates (overkill but take no chance)
                                if vi=="UU":
                                    varstock[vi][i[5:21]] = str(0.514*float(vari.split(" ")[0])) # store variable value at the given time
                                elif vi=="PR0":
                                    varstock[vi][i[5:21]] = str(float(vari)*1000.)
                                elif vi=="HR":
                                    varstock[vi][i[5:21]] = str(float(vari)*100.)
                                else:
                                    varstock[vi][i[5:21]] = vari # store variable value at the given time
                            else:
                                if vi=="UU":
                                    varstock[vi][i[5:21]] = str(0.514*float(vari.split(" ")[0])) # store variable value at the given time
                                elif vi=="PR0":
                                    varstock[vi][i[5:21]] = str(float(vari)*1000.)
                                elif vi=="HR":
                                    varstock[vi][i[5:21]] = str(float(vari)*100.)
                                else:
                                    varstock[vi][i[5:21]] = vari # store variable value at the given time
                else:
                    mailerrors[staname] = [logmsg,datetime.datetime.utcnow().strftime("%Y/%m/%d at %H:%M:%S")] # stores error
                    continue # keep on working

            eq = []
            for k in varstock.keys():
                eq.append(len(set(varstock[k])))
            if not chequal(eq):
                """
                    For now, if any variable is missing even for one time step,
                    the program records an error, returns it, and continues.
                """
                print eq
                print "Not all the variables are available for {0}. Check data.".format(staname)
                errvar = "Variables Error."
                mailerrors[staname] = [errvar,datetime.datetime.utcnow().strftime("%Y/%m/%d at %H:%M:%S")]
                with open(logfile,'a') as L:
                    L.write("Station {0} : data check: {1}, URL acces: {2}, variables retrieval: {3} | "\
                          .format(staname,errdat,logmsg,errvar))
                continue

            else:
                errvar = "OK"
                for k in varstock.keys():
                    for j in set(varstock[k]):
                        if j in Output:
                            Output[j].append(varstock[k][j])
                        else:
                            Output[j] = [varstock[k][j]]
            try:
                dates = [datetime.datetime.strptime(ts, "%Y-%m-%dT%H:%M") for ts in Output.keys()]
                dates.sort() # Sorts the dates from output dictionary
                sordates = [datetime.datetime.strftime(ts,"%Y-%m-%dT%H:%M") for ts in dates] # list of sorted dates
            except:
                errvar = "Error occurred when sorting the data."
                mailerrors[staname] = [datetime.datetime.utcnow().strftime("%Y/%m/%d at %H:%M:%S"),errvar]
                continue

            print "---------------"

            with open(OuF,'a') as F: # Writes Data on output files
                for key in sordates: # write new rows to the file
                    try:
                        keyloc = tmconv(datetime.datetime.strptime(key, "%Y-%m-%dT%H:%M"),True).strftime("%Y/%m/%dT%H")
                        da = tmconv(datetime.datetime.strptime(key, "%Y-%m-%dT%H:%M"),True).strftime("%Y/%m/%d")
                        hou = datetime.datetime.strptime(key, "%Y-%m-%dT%H:%M").strftime("%H")
                        dhou = tmconv(datetime.datetime.strptime(key, "%Y-%m-%dT%H:%M"),True).strftime("%H:%M")
                        rdt = tmconv(startime,True).strftime("%Y/%m/%dT%H")
                        F.write("{0},{1},{2},{3},{4},{5},{6},{7},{8}\n".format(da, \
                        dhou,str(Output[key][3]),str(Output[key][2]),str(Output[key][6]), \
                        str(Output[key][4]),str(Output[key][5]),str(Output[key][0]), \
                        str(Output[key][1])))
                    except:
                        errvar = "Variables Error."
                        continue
                if errvar=="OK" and errdat=="OK" and logmsg=="OK":
                    print "File " + staname + " created/updated successfully" # black and white version
                else:
                    print "File " + staname + " may have been UNSUCCESSFULLY created/updated. (see log file for details)" # black and white version

            with open(logfile,'a') as L:
                L.write("Station {0} : data check: {1}, URL acces: {2}, variables retrieval: {3} | "\
                      .format(staname,errdat,logmsg,errvar))

        with open(logfile,'a') as L:
            L.write("\nDownload finished on "+datetime.datetime.strftime(datetime.datetime.now(),"%Y/%m/%d at %H:%M:%S") + \
            " (local time)")

        if len(mailerrors)>0:
            ml.email_alert(ml.alert_msg(mailerrors,startime),"lecalex@gmail.com")
else:
    sys.exit("Cannot find the file containing virtual stations list.")

sys.exit()
