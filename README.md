% - % - % - % - % - % - % - % - % - % - % - %  

HRDPS time series Downloader  
Converts WMS Data for use  

Created by Alexandre Leca @ IRDA  
HRDPS by Environment Canada @ RPN (CMC)  
__last edit : February 23 2016__  

__IMPORTANT NOTE:__ this code is stable but not heavily tested yet for systematic use.  
Contact <alexandre.leca@irda.qc.ca> for more info and specific details.  

% - % - % - % - % - % - % - % - % - % - % - %  

# README #

This file gives details about the HRDPS WMS download script

### What is this repository for? ###

* Quick summary : sharing the downloading system with our collaborators
* Version 1.1 beta

### What does this script do? ###

* __WMS.py__ creates a GeoMet WMS GetFeatureInfo request for each variable of interest and for each location (ID,lat,lon stored in a separate file) for a selected amount of time (nhour). It stores the files in a backup directory.

    Units are corrected if needed :
    
  * **Temperature** : TT in °C [no change]
  * **Relative Humidity** : HR in no_unit [converted to %]
  * **Wind speed** : UU first part in knots [converted to m.s-1]
  * **Precipitations** : PR0 in m [converted to mm]
  * **Global Radiation** : FB in W.m-2
  * **Direct Radiation** : FSF in W.m-2
  * **Diffuse Radiation** : FSD in W.m-2

* __Date_Sort.py__ sorts the file by date, removes duplicates, and stores data to another directory and also send it to IRDA FTP to be used by RIMpro.


### Arguments Have to be called, as follows : ###

* __WMS.py Path DatOVR__ :
  * _Path_ = Path-to-directory-containing-the-Stations-List(will create or implement a Stations subdirectory to write/append the downloaded data files)
  * _DatOVR_ = argument to override date selection. If no override, set DatOVR to OK, else Enter date to a format YYYY-mm-ddTHH


* __Date_Sort.py Path FTPath__ :  
  * _Path_ = Path_to_directory_containing_the_Stations_List
  * _FTPath_ = Path_to_Output_Files_Storage
