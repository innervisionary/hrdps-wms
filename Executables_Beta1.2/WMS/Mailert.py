#  -*-coding: utf-8 -*-

# - # - # - # - # - # - # - # - # - # - # - #
#         E-Mail alert system for WMS       #
#      Sends an e-mail if errors occur      #
#                                           #
#     Created by Alexandre Leca @ IRDA      #
#   HRDPS by Stephane Belair et al. @ CMC   #
#        last edit : February 17 2016       #
#             -----------------             #
#   Contact alexandre.leca@irda.qc.ca for   #
#      more info and specific details.      #
# - # - # - # - # - # - # - # - # - # - # - #

# Imports
import sys
import os
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

def alert_msg(mail,start_time):
    """
        This function gets the mailerrors dictionary and creates the alert message to
        be sent to the administrator after each download.
    """
    msg = """Hello,

    this is an automatic e-mail sent to inform that an error occurred in the
    download of the HRDPS data files started at {0}: \n""".format(start_time)
    for k in mail.keys():
        byst = "- Station {0} : Error message : '{1}' appeared on  {2} \n".format(\
               k,mail[k][0],mail[k][1])
        msg = msg+byst
    tail = """\nPlease do not answer this e-mail.

    If you need to contact the HRDPS retrieval system administrator :
    prenom.nom@irda.qc.ca / (+1) (tel) tel-telt #ext

    Thank you and have a nice day.

    - IRDA HRDPS-WMS Retrieval Services"""
    msg = msg + tail
    return msg

def email_alert(mail_body,user_address):
    """
        This function sends an e-mail from a Gmail address to inform about a problem
        occurring on file download.
    """
    #user_address = "lecalex@gmail.com"
    password = "hnzmrghdvyinfors"#raw_input("Enter e-mail password (press Enter when finished) :")

    expedit = "lecalex@gmail.com"
    message = MIMEMultipart()
    message['From'] = expedit
    message['To'] = user_address
    message['Subject'] = "HRDPS-WMS Problem Advisory"
    message.attach(MIMEText(mail_body,'plain'))

    server = smtplib.SMTP('smtp.gmail.com',587) # code 465 for SSL, 587 for TLS
    server.starttls()
    server.login("lecalex@gmail.com",password)
    text = message.as_string()
    server.sendmail("lecalex@gmail.com",user_address,text)
    server.quit()
