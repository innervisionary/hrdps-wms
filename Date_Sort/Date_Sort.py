#  -*-coding: utf-8 -*-

# - # - # - # - # - # - # - # - # - # - # - #
#        HRDPS time series Downloader       #
#         Sorts dates and copy to FTP       #
#                                           #
#     Created by Alexandre Leca @ IRDA      #
#   HRDPS by Stephane Belair et al. @ CMC   #
#        last edit : January 15 2016        #
#             -----------------             #
#    IMPORTANT NOTE: this code is stable    #
#    but not heavily tested yet for         #
#    systematic use.                        #
#   Contact alexandre.leca@irda.qc.ca for   #
#      more info and specific details.      #
# - # - # - # - # - # - # - # - # - # - # - #

# Libraries import
import os
import sys
import datetime # date/time formats management
import dateutil
import csv
from shutil import copy
import fileinput

# GETS Arguments for Input/Output file name and path
args = []
for arg in sys.argv:
    args.append(arg)
Path = args[1] # path to read files, in the form : \\192.168.2.4\pomme\Projets\mouillure\Programmation\HRDPS_Parser
FTPath = args[2] # path to the IRDA FTP directory in the form \\192.168.0.7\HRDPS\2016_FCT
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

Path_i = Path + "\\Stations"
Path_o = Path + "\\FCT Files"
if not os.path.exists(Path_o):
    os.makedirs(Path_o)

logf = Path+"\\connect.log"

with open(logf,'a') as L:
    L.write("/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-\n")
    L.write("FTP export of files started on "+ \
            datetime.datetime.strftime(datetime.datetime.now(),"%Y/%m/%d at %H:%M:%S" + "\n"))
    print "FTP export of files started on "+ \
            datetime.datetime.strftime(datetime.datetime.now(),"%Y/%m/%d at %H:%M:%S")

InF = []
for (Path,Dirs,Files) in os.walk(Path_i):
    InF.extend(Files)

for fil in InF:
    IP = Path_i + "\\" + fil
    OP = Path_o + "\\" + fil
    RDR = dict()
    with open(IP,'r') as F:
        dialect = csv.Sniffer().sniff(F.read(2048))
        F.seek(0)
        rdr = csv.reader(F,dialect)
        ncol = len(rdr.next())+1 #9 columns + 1 to make it work
        LINES = [i for i in rdr]

    lin = [i[2:ncol-1] for i in LINES]
    date = [datetime.datetime.strptime(i[0]+","+i[1],"%Y/%m/%d,%H:%M") for i in LINES]
    for j in xrange(0,len(date)):
        RDR[date[j]] = lin[j]
    date.sort()

    # Write sorted data
    with open(OP,'w') as F:
        F.write("DATE,TIME,AIRTEMP,AIRHUM,RAIN,LIGHTWATT,WINDSPEED,RDIFFUSE,RDIRECT\n")
        for k in date:
            F.write(datetime.datetime.strftime(k,"%Y/%m/%d,%H:%M"))
            for i in RDR[k]:
                F.write(",")
                F.write(i)
            F.write("\n")

    # Remove duplicates in the file
    seen = set()
    for line in fileinput.FileInput(OP,inplace=1):
        if line in seen:
            continue # skip duplicate
        seen.add(line)
        print line,

    # Copy file to the FTP
    Success = True
    try:
        copy(OP,FTPath+"\\"+fil)
    except:
        Success = False


    with open(logf,'a') as L:
        if Success:
            L.write("Success: "+fil+" correctly updated to FTP\n")
        else:
            L.write("ERROR: "+fil+" NOT correctly updated to FTP!\n")
            print fil + "encountered an update issue on " + datetime.datetime.now().strftime("%Y/%m/%d at %H:%M:%S")

print "FTP export of files finished on "+ \
        datetime.datetime.strftime(datetime.datetime.now(),"%Y/%m/%d at %H:%M:%S")
